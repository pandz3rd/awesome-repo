package com.tebakkata;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // daftar quiz
        Quiz listQuiz[] = new Quiz[5];
        listQuiz[0] = new Quiz("Siapa nama Jokowi?", "jokowi");
        listQuiz[1] = new Quiz("Siapa nama Prabowo?", "prabowo");
        listQuiz[2] = new Quiz("Siapa nama Megawati?", "megawati");
        listQuiz[3] = new Quiz("Siapa nama saya?", "emboh");
        listQuiz[4] = new Quiz("Siapa nama kamu?", "ya");

        // random question
        int randIndexQuestion = getIndexRandom();

        // membuat scanner
        Scanner scan = new Scanner(System.in);

        // to make array
        char[] splitAnswer = listQuiz[randIndexQuestion].getAnswer().toCharArray();

        String xWord = makeX(listQuiz[randIndexQuestion].getAnswer());
        char[] xWordList = xWord.toCharArray();

        // string builder
        StringBuilder sb = new StringBuilder();

        // ============================================
        // Print
        System.out.println("Question : " + listQuiz[randIndexQuestion].getQuestion());

        System.out.println("Answer : " + xWord);

        int life = 10;
        boolean isEnd = false;

        while (!isEnd && life > 0) {
            System.out.println("Kesempatan Menjawab : " + life);
            System.out.print("Input Jawaban : ");
            String jawaban = scan.next();
            life -= 1;

            // set sb to empty
            sb.setLength(0);

            String correctText = "SALAH!";


            // print each char
            for (int i = 0; i < splitAnswer.length; i++) {
                if (splitAnswer[i] == jawaban.toLowerCase().charAt(0)) {
                    xWordList[i] = jawaban.toLowerCase().charAt(0);
                    correctText = "BENAR!";
                }
                sb.append(xWordList[i]);
            }
            System.out.println(correctText);

            // gabung dari array ke string
            System.out.println("Answer: " + sb);
            System.out.println();

            if(sb.indexOf("*") < 0){
                isEnd = true;
                System.out.println("YOU WON!!!");
            }

            if (life < 1) {
                System.out.println("YOU LOSEEEE!!!");
            }
        }
    }

    public static int getIndexRandom() {
        Random rand = new Random();
        return rand.nextInt(5);
    }

    public static String makeX(String word) {
        String xWord = "";
        for (int i = 0; i < word.length(); i++) {
            xWord = xWord + "*";
        }
        return xWord;
    }
}
