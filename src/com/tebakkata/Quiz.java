package com.tebakkata;

public class Quiz {
    private String question;
    private String answer;

    public Quiz(String q, String a) {
        this.question = q;
        this.answer = a;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }
}
